---
title: "Herunterladen von iCloud Photos"
summary: Herunterladen von iCloud Photos von der Kommandozeile
date: 2021-10-10
weight: 1
aliases: ["/download-icloud-photos"]
tags: ["iCloud", "Apple", "Python", "pip"]
author: "Jan Hoelscher"
---

# Das Problem
Von Zeit zur Zeit kann es vorkommen, dass man alle Fotos aus der iCloud herunterladen möchte. Häufig können sich dort über die Zeit mehrere tausende Fotos ansammeln, iCloud limitiert den Download allerdings auf [1000 Fotos](https://www.techbout.com/download-all-photos-icloud-pc-6859/#:~:text=The%20only%20way%20to%20download,Windows'%20program%20on%20your%20computer.) auf einmal. 

Auch das gleichzeitige Herunterladen von mehreren 1000er Paketen gleichzeitig wird nicht unterstützt. 

# Die Lösung

Nach kurzer Suche auf GitHub liegt die Lösung vor: [icloudpd](https://github.com/icloud-photos-downloader/icloud_photos_downloader). 

Das Tool scheint gut gepflegt und funktionierte in meinem Fall einwandfrei. Die Installation ist mittels `pip3` schnell erledigt.
```
pip3 install icloudpd
```

Der Download aller Fotos sieht wie folgt aus:

```
icloudpd -d targetdir/ -u user.name@icloud.com -p "Passw0rd" 
2021-10-10 16:35:57 DEBUG    Authenticating...
2021-10-10 16:35:58 INFO     Two-step/two-factor authentication is required!
  0: SMS to ********12
  1: SMS to *********34
  2: Enter two-factor authentication code
Please choose an option: [0]: 2
Please enter two-factor authentication code: 123456
2021-10-10 16:36:20 INFO     Great, you're all set up. The script can now be run without user interaction until 2SA expires.
You can set up email notifications for when the two-step authentication expires.
(Use --help to view information about SMTP options.)
2021-10-10 16:36:21 DEBUG    Looking up all photos and videos from album All Photos...
2021-10-10 16:36:21 INFO     Downloading 9999 original photos and videos to myphotos ...
< Here comes your progress bar >
2021-10-10 17:04:00 INFO     All photos have been downloaded!
```

## Die Sache mit dem Bildformat

Apple nutzt für seine Fotos zum Teil das `HEIC` [Format](https://de.wikipedia.org/wiki/High_Efficiency_Image_File_Format). Dies ist leider nicht so kompatibel wie `jpeg`. Jedoch lassen sich unter Linux die Fotos mit zwei kleinen Kommandos konvertieren. 

```
# Installieren vom Converter
sudo apt install libheif-examples

# Konvertieren aller Fotos im HEIC Format
for file in $(find -type f -name "*.HEIC"); do heif-convert $file ${file/%.HEIC/.JPG}; done
```
 

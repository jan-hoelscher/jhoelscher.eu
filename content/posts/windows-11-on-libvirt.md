---
title: "Windows 11 auf libvirt"
summary: Installation von Windows 11 auf libvirt
date: 2021-10-09
weight: 1
aliases: ["/windows-11-auf-libvirt"]
tags: ["libvirt", "Virtualisierung", "Windows"]
author: "Jan Hoelscher"
---

Nachdem Windows 11 am 05.10.2021 [veröffentlicht](https://www.heise.de/news/Windows-11-Neue-Betriebssystemversion-jetzt-allgemein-verfuegbar-6207571.html) wurde, wollte ich das ganze in einer virtuellen Maschine austesten. 

Für gewöhnlich nutze ich dafür [libvirt](https://libvirt.org/index.html) unter Linux, dort gab es allerdings einige Probleme durch die neuen [Hardwareanforderung von Windows 11](https://aka.ms/WindowsSysReq).

Die folgenden Punkte haben einer einfachen Installtion im Weg gestanden: 

- System Firmware "UEFI, Secure Boot capable."
- TPM "Trusted Platform Module (TPM) version 2.0"

# System Firmware

Dieses Problem war relativ leicht zu lösen, allerdings muss man diese Einstellung beim Erstellen der Maschine setzten. Eine nachträgliche Änderung ist nicht mehr möglich. 

Dazu muss im letzten Schritt des anlegens der VM vom VMM (Virtual Maschine Manager) "Konfiguration bearbeiten vor der Installation" gewählt werden. Danach kann im Punkt "Übersicht" als Firmware der Eintrag mit "UEFI" augewählt werden.

# TPM 

Ein sogenanntes "Trusted Platform Module" ist dafür da, die Sicherheit zu erhöhen. Er soll z.B. gegen Manipulation schützen. Ab Windows 11 ist der Einsatz in virtuellen Maschinen unumgänglich. 

Es gibt die Möglichkeit das TPM des Wirts an die virtuelle Maschine weiterzugeben (passtrough). Dies ist allerdings in vielen Fällen nicht sehr verlässlich und auch nicht umbedingt portabel. Die Alternative ist der Einsatz einen Software TPM. 

Zu diesem Zweck haben zwei IBM Mitarbeiter das Tool [swtpm](https://github.com/stefanberger/swtpm) entwickelt. Die Möglichkeiten des Tools werden im folgenden kurz beschrieben: 

> The SWTPM package provides TPM emulators with different front-end interfaces to libtpms. TPM emulators provide socket interfaces (TCP/IP and Unix) and the Linux CUSE interface for the creation of multiple native /dev/vtpm devices.

Mithilfe dieses Tools lassen sich einfach "virtuelle" TPMs in virtuelle Maschinen einbinden. Innerhalb einer libvirt Konfiguration sieht es wie folgt aus:
```
<tpm model="tpm-crb">
  <backend type="emulator" version="2.0"/>
</tpm>

```

Im Anschluss erkennt Windows das TPM Modul in lässt sich installieren. 

## Probleme mit offiziellen Paketquellen

Für den Einsatz eines virtuellen TPMs wird das Binary `swtpm` benötigt, die Installation hat auf meinem System leider nicht geklappt. Ich habe als Wirt System ein Ubuntu 21.04 benutzt, es scheint das der Fehler allerdings auch auf Ubuntu 20.04 auftritt. 

Die derzeitige Version in den offiziellen Ubuntu Paketquellen ist leider veraltet. 

```
$ apt info libtpms0

Package: libtpms0
Version: 0.8.0~dev1-1.2ubuntu1
Priority: optional
Section: universe/libs
Source: libtpms
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Seunghun Han <kkamagui@gmail.com>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 976 kB
Depends: openssl, libc6 (>= 2.17), libssl1.1 (>= 1.1.1)
Homepage: https://github.com/stefanberger/libtpms
Download-Size: 317 kB
APT-Sources: http://de.archive.ubuntu.com/ubuntu hirsute/universe amd64 Packages
Description: TPM emulation library
 Libtpms is a library that provides TPM functionality. Libtpm is used
 by swtpm package.
```

Aktuell (Stand: 10.10.2021) liegen `swtpm` in der Version 0.6.1 und `libtpms` in der Version 0.9.0 vor. Die Installation ist in den GitHub Repos gut dokumentiert. 

- https://github.com/stefanberger/swtpm
- https://github.com/stefanberger/libtpms

Kurz zusammengefasst kann man entweder lokal die `libtpms` Library und `swtpm` Binary kompilieren oder ein `.deb` Paket bauen. Ich habe mich für das lokale kompilieren entschieden. 

Zunächst muss die Library mit den folgenden Befehlen kompiliert werden:
```
sudo apt install automake autoconf libtool make gcc libc-dev libssl-dev

wget https://github.com/stefanberger/libtpms/archive/refs/tags/v0.9.0.zip
unzip v0.9.0.zip

cd libtpms-0.9.0
./autogen.sh --with-tpm2 --with-openssl --prefix=/usr
make
make check
sudo make install
```

Im Anschluss dann noch das Binary kompilieren:
```
sudo apt install build-essential devscripts equivs

wget https://github.com/stefanberger/swtpm/archive/refs/tags/v0.6.1.zip
unzip v0.6.1.zip

cd swtpm-0.6.1

sudo mk-build-deps --install ./debian/control

./autogen.sh --prefix=/usr 
make
make check
sudo make install
```

Um kurz zu überprüfen, ob das Binary korrekt installiert ist bietet sich folgender Befehl an: 

```
swtpm --version
TPM emulator version 0.6.1, Copyright (c) 2014-2021 IBM Corp.
```

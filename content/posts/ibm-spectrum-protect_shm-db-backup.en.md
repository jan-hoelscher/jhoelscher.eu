---
title: "Spectrum Protect - Backup over Shared Memory"
summary: Backup over Shared Memory in Spectrum Proctet
date: 2020-07-08
series: ["IBM Spectrum Protect"]
weight: 1
aliases: ["/spectrum-protect-shared-memory-db-backup"]
tags: ["IBM Spectrum Protect", "Backup", "Performance"]
author: "Jan Hoelscher"
---

IBM Spectrum Protect normally uses a connection over TCPIP to backup its database. In this article i will show you how to change connection to Shared Memory. Changes has to be made in configuration from server instance and a special `dsm.sys`. 

{{< notice note >}}
This option was introduced in version 7.1 of Tivoli Storage Manager.
{{< /notice >}}

## Server
The serverinstance is configured by a File in home directory which is called `dsmserv.opt`

The following lines must be added:
```
COMMMethod  SHAREdmem
SHMPort 1510
```

The server instance must be restarted for the changes to take effect.

After this you can check with Telnet if the port is opened and reachable.
```
# telnet localhost 1510
Trying... 
Connected to localhost.
Escape character is '^]'.
```

At this point only the server side is opened, but database backup uses still TCPIP as commucation method.

## Database Backup Client
Database backup uses a special Node called `$$_TSMDBMGR_$$`, options are stored in a `dsm.sys` file like a normal Backup-Archive Client.

This special file can be found at the following path:
```
# AIX 
/opt/tivoli/tsm/server/bin/dbbkapi/dsm.sys
```

Diese sollte folgenden Inhalt aufweisen:
Following lines shoud be in this file:
```
COMMMethod      SHAREdmem
SHMPort         1510
NODENAME        $$_TSMDBMGR_$$
PASSWORDDIR     <HOME of instance user>
ERRORLOGNAME    <HOME of instance user>/tsmdbmgr.log
```

## Performance
I have expirenced a speedup of 2 times faster than using TCPIP. In addition to that i have opserved that throughput is more constant, there a no slow individual database backups.

---
title: "Ansible Variablen basierend auf Hostnamen erzeugen"
summary: Ansible Varialben basierend auf Hostnamen erzeugen
date: 2021-11-04
weight: 1
aliases: ["/ansible-var-based-on-hostname"]
tags: ["quicktipp", "Ansible"]
author: "Jan Hoelscher"
cover:
    image: "ansible-var-based-on-hostname.jpg"
---

# Das Problem

Ansible Inventorys sind eine tolle Sache. Man hat die Möglichkeit externe Datenbanken als Quelle zu nutzen, aber dazu etwas später. Gerade wenn man anfängt mit Ansible zu arbeiten ist es häufig nicht das erste Ziel die zentrale CMDB oder ähnliches anzubinden. 

Jedoch existieren häufig innerhalb einer Gruppe von Servern bestimmte Namenskonventionen, die man dafür nutzen kann zusätzliche Attribute zu erzeugen.

# Die Lösung

In meinem Fall möchte ich gerne mittels Ansible die vorhandenen ESX Server im Fibre-Channel Netzwerk zonen. Die Server werden abhängig von ihrem Standort gezoned, sprich abhängig vom RZ. 

Das Inventory vom Ansible sieht wie folgt aus: 

```
[esxserver]
esx01.example.org
esx02.example.org
esx03.example.org
```

Glücklicherweise haben wir eine einfache Namenskonvention um direkt zu erkennen wo ein ESX Server verbaut ist. Ist die Nummer ungerade, ist es RZ1 - ist sie ungerade RZ2 (wie bei Hausnummern)

Um nun anhand des Hostnames eine Variable festzulegen muss ein bisschen Jinja2 Magie innerhalb Ansible verwendet werden: 

```
- name: Set RZ Location based on Hostname
  set_fact:
   rz: "{{ 'rz2' if inventory_hostname_short[-2:] | int is divisibleby 2 else 'rz1' }}"
```

In diesem Beispiel wird nicht der [FQDN](https://de.wikipedia.org/wiki/Fully-Qualified_Host_Name), sondern die spezielle Variable `invetory_hostname_short` genutzt. Somit wird aus `esx01.example.org` - `esx01`. 
Im zweiten Schritt wird mittels Substring die Nummer des ESX Server aus dem Hostname extrahiert. 
Als letztes wird mittels Jinja2 Conditionals und Filter geprüft, ob die Nummer gerade durch zwei geteilt werden kann. Abhängig vom Ergebnis, ob `true` oder `false` wird die Variable auf `RZ1` oder `RZ2` gesetzt. 

Das Prinzip kann für etliche Bedindungen verwendet werden, zum Beispiel könnte abhängig vom Hostname eine Applikationsvariable gesetzt werden oder welcher Umgebung (Prod, Test, Dev) der Server angehört. 

Header Credit: [CC-BY-SA Erik Wilde](https://www.flickr.com/photos/dret/14354341549)
